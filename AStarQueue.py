
#from Queue import Queue
from collections import deque


class AStarNode:
    def __init__(self, item):
        self.prev = None
        self.next = None
        self.item = item

    def __str__(self):
        return str(self.item)


class AStarQueue:
    def __init__(self):
        self.allnodes = {}
        self.head = None
        self.tail = None
        self.priorities = {}
        self.len = 0

    def __len__(self):
        return self.len

    def __str__(self):
        string = ""
        node = self.head
        while node:
            string += str(node.item)
            string += "\ncost: {}\n\n".format(node.item.cost)
            node = node.next
        string += "\n\nPriorities: "
        string += ", ".join(map(str, sorted(self.priorities.keys())))
        return string

    def add(self, item):
        ret = False
        key = hash(item)
        if key in self.allnodes:
            if self.allnodes[key].item.depth > item.depth:
                #New node has a shorter depth than previously existing node
                #update parent of existing node
                oldnode = self.allnodes[key]
                oldnode.item.parent.successors.remove(key)
                oldnode.item.parent = item.parent
                oldnode.item.parent.successors += [key]
                oldnode.item.addancestors()
                #remove all affected nodes from linked list
                toremove = deque()
                removed = deque()
                toremove.append(oldnode)
                while toremove:
                    current = toremove.popleft()
                    if current.item.open:
                        removed.append(current)
                        if current.prev:
                            current.prev.next = current.next
                        if current.next:
                            current.next.prev = current.prev
                        if self.head == current:
                            if current.next:
                                self.head = current.next
                                #self.head.prev = None
                            else:
                                self.head = None
                        if self.tail == current:
                            if current.prev:
                                self.tail = current.prev
                                #self.tail.next = None
                            else:
                                self.tail = None
                        if not current.prev or current.prev.item.cost != current.item.cost:
                            if current.next and current.next.item.cost == current.item.cost:
                                #move pointer
                                self.priorities[current.item.cost] = current.next
                            else:
                                #delete pointer
                                #if current.item.cost == 962:
                                #    print("boo1")
                                    #q = self.head
                                    #while q and q != self.tail:
                                    #    if q.item.cost == 962:
                                    #        print("i exist")
                                    #    q = q.next
                                del self.priorities[current.item.cost]
                                #if current.item.cost == 962:
                                #    print("boo2")
                    toremove.extend([self.allnodes[x] for x in current.item.successors])
                    current.item.calccost()
                #reinsert affected nodes after recalculating cost
                for x in removed:
                    self.insert(x)
        else:
            #State doesn't exist, add to struct
            ret = True
            thisnode = AStarNode(item)
            self.allnodes[key] = thisnode
            self.insert(thisnode)
            self.len += 1
        return ret
                
    def insert(self, node):
        node.prev = None
        #node.head = None
        node.next = None
        if self.len == 0:
            self.head = node
            self.tail = node
        else:
            key = hash(node.item)
            nextpriority = -1
            for p in sorted(self.priorities.keys()):
                if p > node.item.cost and self.priorities[p] != None:
                    nextpriority = p
                    break
            if nextpriority > -1:
                nextnode = self.priorities[nextpriority]
                node.next = nextnode
                if nextnode.prev:
                    prevnode = nextnode.prev
                    node.prev = prevnode
                    prevnode.next = node
                else:
                    self.head = node
                nextnode.prev = node
            else:
                self.tail.next = node
                node.prev = self.tail
                self.tail = node
        if not node.prev or node.item.cost != node.prev.item.cost:
            self.priorities[node.item.cost] = node

    def pop(self):
        ret = None
        if self.head:
            node = self.head
            if node.next:
                if node.item.cost == node.next.item.cost:
                    self.priorities[node.item.cost] = node.next
                else:
                    del self.priorities[node.item.cost]
                self.head = node.next
                self.head.prev = None
            else:
                self.head = None
                self.tail = None
                del self.priorities[node.item.cost]
            ret = node.item
            self.len -= 1
        return ret
