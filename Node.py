
#heuristictype = 1

class Node:
    def __init__(self, state, htype, parent, action):
        self.state = state
        self.parent = parent
        self.action = action
        self.open = True
        self.heuristictype = htype
        self.hcost = 0
        self.heuristic()
        self.depth = 0
        self.cost = 0
        self.calccost()
        self.ancestors = set()
        self.successors = []

    def __str__(self):
        return "{}\n{}".format(self.action, str(self.state))

    def __eq__(self, other):
        return self.state == other.state

    def __hash__(self):
        return hash(self.state)

    def changeheuristic(self, newtype):
        global heuristictype
        heuristictype = newtype

    def heuristic1(self):
        return self.state.heuristic1()

    def heuristic2(self):
        return self.state.heuristic2()

    def heuristicavg(self):
        return (self.state.heuristic1()+self.state.heuristic2())/2

    def heuristic(self):
        hvalue = 0
        if self.heuristictype == 1:
            hvalue = self.heuristic1()
        elif self.heuristictype == 2:
            hvalue = self.heuristic2()
        else:
            hvalue = self.heuristicavg()
        self.hcost = hvalue
        return hvalue

    def calccost(self):
        if self.parent:
            self.depth = self.parent.depth + self.state.actioncost
        self.cost = self.depth + self.hcost

    def solved(self):
        return self.state.solved()

    def addsuccessor(self, succ):
        self.successors += [hash(succ)]

    def addancestors(self):
        if self.parent:
            self.ancestors = set(self.parent.ancestors)
            self.ancestors.add(hash(self.parent))

    def actions(self):
        newnodes = []
        for i in self.state.actions():
            newnodes += [Node(i[1], self.heuristictype, self, i[0])]
        return newnodes
