#numbers of cars to let through for different light times
waittimes = [5,10,15]
#time light takes to change
changetime = 5
#time car takes to pass through junction
cartime = 1
#are cars allowed to turn right on red lights if it is safe
rightonred = True

class Lanes:
    def __init__(self, oldlanes=None):
        if oldlanes:
            self.north = oldlanes.north[:]
            self.northleft = oldlanes.northleft[:]
            self.east = oldlanes.east[:]
            self.eastleft = oldlanes.eastleft[:]
            self.south = oldlanes.south[:]
            self.southleft = oldlanes.southleft[:]
            self.west = oldlanes.west[:]
            self.westleft = oldlanes.westleft[:]
        else:
            self.north = []
            self.northleft = []
            self.east = []
            self.eastleft = []
            self.south = []
            self.southleft = []
            self.west = []
            self.westleft = []

    def carsleft(self):
        return ( len(self.north) + len(self.northleft) +
                 len(self.east) + len(self.eastleft) +
                 len(self.south) + len(self.southleft) +
                 len(self.west) + len(self.westleft) )

    def timestring(self, time):
        width = len(str(max(len(self.north),len(self.northleft),
                            len(self.east),len(self.eastleft),
                            len(self.south),len(self.southleft),
                            len(self.west),len(self.southleft))))
        northpresent = len(filter(lambda x: x[0]<=time, self.north))
        northleftpresent = len(filter(lambda x: x[0]<=time, self.northleft))
        eastpresent = len(filter(lambda x: x[0]<=time, self.east))
        eastleftpresent = len(filter(lambda x: x[0]<=time, self.eastleft))
        southpresent = len(filter(lambda x: x[0]<=time, self.south))
        southleftpresent = len(filter(lambda x: x[0]<=time, self.southleft))
        westpresent = len(filter(lambda x: x[0]<=time, self.west))
        westleftpresent = len(filter(lambda x: x[0]<=time, self.westleft))
        
        ret = " "*width*2 + " "
        ret += " {a: >{w}} {b: >{w}}\n".format(a=len(self.north)-northpresent,
                                            b=len(self.northleft)-northleftpresent,
                                            w=width)
        ret += " "*width*2 + " "
        ret += " {a: >{w}} {b: >{w}}\n".format(a=northpresent,
                                            b=northleftpresent,
                                            w=width)
        ret += "{a: >{w}} {b: >{w}} ".format(a=len(self.westleft)-westleftpresent,b=westleftpresent,w=width)
        ret += " "*width*2
        ret += "  {a: >{w}} {b: >{w}}\n".format(a=eastpresent,b=len(self.east)-eastpresent,w=width)
        ret += "{a: >{w}} {b: >{w}} ".format(a=len(self.west)-westpresent,b=westpresent,w=width)
        ret += " "*width*2
        ret += "  {a: >{w}} {b: >{w}}\n".format(a=eastleftpresent,b=len(self.eastleft)-eastleftpresent,w=width)
        ret += " "*width*2 + " "
        ret += " {a: >{w}} {b: >{w}}\n".format(a=southleftpresent,
                                               b=southpresent,
                                               w=width)
        ret += " "*width*2 + " "
        ret += " {a: >{w}} {b: >{w}}\n".format(a=len(self.southleft)-southleftpresent,
                                               b=len(self.south)-southpresent,
                                               w=width)
        return ret

    def timecutoff(self, time):
        newlanes = Lanes(self)
        newlanes.north = filter(lambda x: x[0]<=time, newlanes.north)
        newlanes.northleft = filter(lambda x: x[0]<=time, newlanes.northleft)
        newlanes.east = filter(lambda x: x[0]<=time, newlanes.east)
        newlanes.eastleft = filter(lambda x: x[0]<=time, newlanes.eastleft)
        newlanes.south = filter(lambda x: x[0]<=time, newlanes.south)
        newlanes.southleft = filter(lambda x: x[0]<=time, newlanes.southleft)
        newlanes.west = filter(lambda x: x[0]<=time, newlanes.west)
        newlanes.westleft = filter(lambda x: x[0]<=time, newlanes.westleft)
        return newlanes

    def __str__(self):
        width = len(str(max(len(self.north),len(self.northleft),
                            len(self.east),len(self.eastleft),
                            len(self.south),len(self.southleft),
                            len(self.west),len(self.southleft))))
        ret = " "*width
        ret += " {a: >{w}} {b: >{w}}\n".format(a=len(self.north),
                                            b=len(self.northleft),
                                            w=width)
        ret += "{a: >{w}} ".format(a=len(self.westleft),w=width)
        ret += " "*width*2
        ret += "  {a: >{w}}\n".format(a=len(self.east),w=width)
        ret += "{a: >{w}} ".format(a=len(self.west),w=width)
        ret += " "*width*2
        ret += "  {a: >{w}}\n".format(a=len(self.eastleft),w=width)
        ret += " "*width
        ret += " {a: >{w}} {b: >{w}}\n".format(a=len(self.southleft),
                                               b=len(self.south),
                                               w=width)
        return ret

    def __eq__(self, other):
        return ( self.north == other.north and self.northleft == other.northleft and
                 self.east == other.east and self.eastleft == other.eastleft and
                 self.south == other.south and self.southleft == other.southleft and
                 self.west == other.west and self.westleft == other.westleft )


class State:
    def __init__(self, lanes, config, cost, time):
        self.lanes = lanes
        self.config = config
        self.time = time
        self.actioncost = cost

    def __str__(self):
        return self.lanes.timestring(self.time)

    def __eq__(self, other):
        return ( self.config == other.config and
                 self.time == other.time and
                 self.lanes == other.lanes )

    def __hash__(self):
        string = "{}:{}:".format(self.time, self.config)
        string += "{},".format(len(self.lanes.north))
        string += "{},".format(len(self.lanes.northleft))
        string += "{},".format(len(self.lanes.east))
        string += "{},".format(len(self.lanes.eastleft))
        string += "{},".format(len(self.lanes.south))
        string += "{},".format(len(self.lanes.southleft))
        string += "{},".format(len(self.lanes.west))
        string += "{}".format(len(self.lanes.westleft))
        return hash(string)

    def solved(self):
        return self.lanes.carsleft() == 0

    def goalstr(self):
        ##
        ret = ""
        for x in range(self.height):
            for y in range(self.width):
                ret += "{:>3}".format(self.goal[x*self.width+y])
            ret += "\n"
        return ret

    def heuristic2(self):
        total = 0
        total += self.lanes.carsleft()*20
        return total

    def heuristic11(self):
        total = 0
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.north)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.northleft)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.east)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.eastleft)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.south)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.southleft)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.west)))
        total += sum(map(lambda y: (self.time-y[0])**2,
                         filter(lambda x: x[0]<=self.time,
                                self.lanes.westleft)))
        return total

    def heuristic1(self):
        total = 0
        for i in range(len(self.lanes.north)):
            if self.lanes.north[i][0] <= self.time:
                total += self.time - self.lanes.north[i][0]
            else:
                break
        for i in range(len(self.lanes.northleft)):
            if self.lanes.northleft[i][0] <= self.time:
                total += self.time - self.lanes.northleft[i][0]
            else:
                break
        for i in range(len(self.lanes.east)):
            if self.lanes.east[i][0] <= self.time:
                total += self.time - self.lanes.east[i][0]
            else:
                break
        for i in range(len(self.lanes.eastleft)):
            if self.lanes.eastleft[i][0] <= self.time:
                total += self.time - self.lanes.eastleft[i][0]
            else:
                break
        for i in range(len(self.lanes.south)):
            if self.lanes.south[i][0] <= self.time:
                total += self.time - self.lanes.south[i][0]
            else:
                break
        for i in range(len(self.lanes.southleft)):
            if self.lanes.southleft[i][0] <= self.time:
                total += self.time - self.lanes.southleft[i][0]
            else:
                break
        for i in range(len(self.lanes.west)):
            if self.lanes.west[i][0] <= self.time:
                total += self.time - self.lanes.west[i][0]
            else:
                break
        for i in range(len(self.lanes.westleft)):
            if self.lanes.westleft[i][0] <= self.time:
                total += self.time - self.lanes.westleft[i][0]
            else:
                break
        return total

    def vertical(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "v":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.north) != 0 and
                newlanes.north[0][0] <= time ):
                cost += time-newlanes.north.pop(0)[0]
                #carmoved = True
            else:
                if ( len(newlanes.southleft) != 0 and
                    newlanes.southleft[0][0] <= time ):
                    cost += time-newlanes.southleft.pop(0)[0]
                    #carmoved = True
                if ( rightonred and
                    len(newlanes.west) != 0 and
                    newlanes.west[0][1] == "s" and
                    newlanes.west[0][0] <= time ):
                    cost += time-newlanes.west.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.south) != 0 and
                newlanes.south[0][0] <= time ):
                cost += time-newlanes.south.pop(0)[0]
                #carmoved = True
            else:
                if ( len(newlanes.northleft) != 0 and
                    newlanes.northleft[0][0] <= time ):
                    cost += time-newlanes.northleft.pop(0)[0]
                    #carmoved = True
                if ( rightonred and
                    len(newlanes.east) != 0 and
                    newlanes.east[0][1] == "n" and
                    newlanes.east[0][0] <= time ):
                    cost += time-newlanes.east.pop(0)[0]
                    #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "v", cost, newtime)
        return ("vertical {}".format(numcars), s)

    def horizontal(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "h":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.east) != 0 and
                newlanes.east[0][0] <= time ):
                cost += time-newlanes.east.pop(0)[0]
                #carmoved = True
            else:
                if ( len(newlanes.westleft) != 0 and
                    newlanes.westleft[0][0] <= time ):
                    cost += time-newlanes.westleft.pop(0)[0]
                    #carmoved = True
                if ( rightonred and
                    len(newlanes.north) != 0 and
                    newlanes.north[0][1] == "w" and
                    newlanes.north[0][0] <= time ):
                    cost += time-newlanes.north.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.west) != 0 and
                newlanes.west[0][0] <= time ):
                cost += time-newlanes.west.pop(0)[0]
                #carmoved = True
            else:
                if ( len(newlanes.eastleft) != 0 and
                    newlanes.eastleft[0][0] <= time ):
                    cost += time-newlanes.eastleft.pop(0)[0]
                    #carmoved = True
                if ( rightonred and
                    len(newlanes.south) != 0 and
                    newlanes.south[0][1] == "e" and
                    newlanes.south[0][0] <= time ):
                    cost += time-newlanes.south.pop(0)[0]
                    #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "h", cost, newtime)
        return ("horizontal {}".format(numcars), s)

    def verticalleft(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "vl":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.northleft) != 0 and
                newlanes.northleft[0][0] <= time ):
                cost += time-newlanes.northleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.south) != 0 and
                    newlanes.south[0][1] == "e" and
                    newlanes.south[0][0] <= time ):
                    cost += time-newlanes.south.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.southleft) != 0 and
                newlanes.southleft[0][0] <= time ):
                cost += time-newlanes.southleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.north) != 0 and
                    newlanes.north[0][1] == "w" and
                    newlanes.north[0][0] <= time ):
                    cost += time-newlanes.north.pop(0)[0]
                    #carmoved = True
            if ( rightonred and
                len(newlanes.east) != 0 and
                newlanes.east[0][1] == "n" and
                newlanes.east[0][0] <= time ):
                cost += time-newlanes.east.pop(0)[0]
                #carmoved = True
            if ( rightonred and
                len(newlanes.west) != 0 and
                newlanes.west[0][1] == "s" and
                newlanes.west[0][0] <= time ):
                cost += time-newlanes.west.pop(0)[0]
                #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "vl", cost, newtime)
        return ("vertical left {}".format(numcars), s)

    def horizontalleft(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "hl":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.eastleft) != 0 and
                newlanes.eastleft[0][0] <= time ):
                cost += time-newlanes.eastleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.west) != 0 and
                    newlanes.west[0][1] == "s" and
                    newlanes.west[0][0] <= time ):
                    cost += time-newlanes.west.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.westleft) != 0 and
                newlanes.westleft[0][0] <= time ):
                cost += time-newlanes.westleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.east) != 0 and
                    newlanes.east[0][1] == "n" and
                    newlanes.east[0][0] <= time ):
                    cost += time-newlanes.east.pop(0)[0]
                    #carmoved = True
            if ( rightonred and
                len(newlanes.south) != 0 and
                newlanes.south[0][1] == "e" and
                newlanes.south[0][0] <= time ):
                cost += time-newlanes.south.pop(0)[0]
                #carmoved = True
            if ( rightonred and
                len(newlanes.north) != 0 and
                newlanes.north[0][1] == "w" and
                newlanes.north[0][0] <= time ):
                cost += time-newlanes.north.pop(0)[0]
                #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "hl", cost, newtime)
        return ("horizontal left {}".format(numcars), s)

    def northpriority(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "np":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.north) != 0 and
                newlanes.north[0][0] <= time ):
                cost += time-newlanes.north.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.west) != 0 and
                    newlanes.west[0][1] == "s" and
                    newlanes.west[0][0] <= time ):
                    cost += time-newlanes.west.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.northleft) != 0 and
                newlanes.northleft[0][0] <= time ):
                cost += time-newlanes.northleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.south) != 0 and
                    newlanes.south[0][1] == "e" and
                    newlanes.south[0][0] <= time ):
                    cost += time-newlanes.south.pop(0)[0]
                    #carmoved = True
            if ( rightonred and
                len(newlanes.east) != 0 and
                newlanes.east[0][1] == "n" and
                newlanes.east[0][0] <= time ):
                cost += time-newlanes.east.pop(0)[0]
                #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "np", cost, newtime)
        return ("north priority {}".format(numcars), s)

    def eastpriority(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "ep":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.east) != 0 and
                newlanes.east[0][0] <= time ):
                cost += time-newlanes.east.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.north) != 0 and
                    newlanes.north[0][1] == "w" and
                    newlanes.north[0][0] <= time ):
                    cost += time-newlanes.north.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.eastleft) != 0 and
                newlanes.eastleft[0][0] <= time ):
                cost += time-newlanes.eastleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.west) != 0 and
                    newlanes.west[0][1] == "s" and
                    newlanes.west[0][0] <= time ):
                    cost += time-newlanes.west.pop(0)[0]
                    #carmoved = True
            if ( rightonred and
                len(newlanes.south) != 0 and
                newlanes.south[0][1] == "e" and
                newlanes.south[0][0] <= time ):
                cost += time-newlanes.south.pop(0)[0]
                #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "ep", cost, newtime)
        return ("east priority {}".format(numcars), s)

    def southpriority(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "sp":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.south) != 0 and
                newlanes.south[0][0] <= time ):
                cost += time-newlanes.south.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.east) != 0 and
                    newlanes.east[0][1] == "n" and
                    newlanes.east[0][0] <= time ):
                    cost += time-newlanes.east.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.southleft) != 0 and
                newlanes.southleft[0][0] <= time ):
                cost += time-newlanes.southleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.north) != 0 and
                    newlanes.north[0][1] == "w" and
                    newlanes.north[0][0] <= time ):
                    cost += time-newlanes.north.pop(0)[0]
                    #carmoved = True
            if ( rightonred and
                len(newlanes.west) != 0 and
                newlanes.west[0][1] == "s" and
                newlanes.west[0][0] <= time ):
                cost += time-newlanes.west.pop(0)[0]
                #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "sp", cost, newtime)
        return ("south priority {}".format(numcars), s)

    def westpriority(self, numcars):
        newlanes = Lanes(self.lanes)
        newtime = self.time
        cost = 0
        localchangetime = 0
        if self.config != "wp":
            localchangetime += changetime
        for time in map(lambda x: self.time+localchangetime+(x*cartime), range(numcars)):
            #carmoved = False
            if ( len(newlanes.west) != 0 and
                newlanes.west[0][0] <= time ):
                cost += time-newlanes.west.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.south) != 0 and
                    newlanes.south[0][1] == "e" and
                    newlanes.south[0][0] <= time ):
                    cost += time-newlanes.south.pop(0)[0]
                    #carmoved = True
            if ( len(newlanes.westleft) != 0 and
                newlanes.westleft[0][0] <= time ):
                cost += time-newlanes.westleft.pop(0)[0]
                #carmoved = True
            else:
                if ( rightonred and
                    len(newlanes.east) != 0 and
                    newlanes.east[0][1] == "n" and
                    newlanes.east[0][0] <= time ):
                    cost += time-newlanes.east.pop(0)[0]
                    #carmoved = True
            if ( rightonred and
                len(newlanes.north) != 0 and
                newlanes.north[0][1] == "w" and
                newlanes.north[0][0] <= time ):
                cost += time-newlanes.north.pop(0)[0]
                #carmoved = True
            #if carmoved:
            newtime = time+1
        s = State(newlanes, "wp", cost, newtime)
        return ("west priority {}".format(numcars), s)

    def actions(self):
        newstates = []
        for t in waittimes:
            new = self.vertical(t)
            newstates += [new]
        for t in waittimes:
            new = self.horizontal(t)
            newstates += [new]
        for t in waittimes:
            new = self.verticalleft(t)
            newstates += [new]
        for t in waittimes:
            new = self.horizontalleft(t)
            newstates += [new]
        for t in waittimes:
            new = self.northpriority(t)
            newstates += [new]
        for t in waittimes:
            new = self.eastpriority(t)
            newstates += [new]
        for t in waittimes:
            new = self.southpriority(t)
            newstates += [new]
        for t in waittimes:
            new = self.westpriority(t)
            newstates += [new]
        return newstates

    def actionswithcheck(self):
        newstates = []
        temp = []
        for t in waittimes:
            new = self.vertical(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.horizontal(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.verticalleft(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.horizontalleft(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.northpriority(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.eastpriority(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.southpriority(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        temp = []
        for t in waittimes:
            new = self.westpriority(t)
            if ( new[1].time != self.time and
                 new[1] not in temp ):
                temp += [new[1]]
                newstates += [new]
        if len(newstates) == 0:
            newstates += [State(Lanes(self.lanes),
                                self.config,
                                self.time+waittimes[0])]
        return newstates


def getinit():
    lanes = Lanes()
    filename = raw_input("input file name:\n>> ")
    if not filename:
        filename = "traffic.csv"
    with open(filename) as f:
        for line in f:
            line.strip()
            line.replace(" ","")
            cars = line.split(",")
            for car in cars:
                if car[-2] == "n":
                    if car[-1] == "w" or car[-1] == "s":
                        lanes.north += [(int(car[:-2]), car[-1])]
                    elif car[-1] == "e":
                        lanes.northleft += [(int(car[:-2]), car[-1])]
                elif car[-2] == "e":
                    if car[-1] == "w" or car[-1] == "n":
                        lanes.east += [(int(car[:-2]), car[-1])]
                    elif car[-1] == "s":
                        lanes.eastleft += [(int(car[:-2]), car[-1])]
                elif car[-2] == "s":
                    if car[-1] == "n" or car[-1] == "e":
                        lanes.south += [(int(car[:-2]), car[-1])]
                    elif car[-1] == "w":
                        lanes.southleft += [(int(car[:-2]), car[-1])]
                elif car[-2] == "w":
                    if car[-1] == "e" or car[-1] == "s":
                        lanes.west += [(int(car[:-2]), car[-1])]
                    elif car[-1] == "n":
                        lanes.westleft += [(int(car[:-2]), car[-1])]
    lanes.north.sort(key=lambda c: c[0])
    lanes.northleft.sort(key=lambda c: c[0])
    lanes.east.sort(key=lambda c: c[0])
    lanes.eastleft.sort(key=lambda c: c[0])
    lanes.south.sort(key=lambda c: c[0])
    lanes.southleft.sort(key=lambda c: c[0])
    lanes.west.sort(key=lambda c: c[0])
    lanes.westleft.sort(key=lambda c: c[0])
    return State(lanes, "init", 0, 0)
