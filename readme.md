How To Use:
In a console run the Main.py file with Python 2.7, and
at the prompt type the name of one of the csv files to
begin simulating traffic.

Running generattraffic.py will allow you to create your
own csv file with differetn traffic conditions.

The iterative A* distance perception can be set inside of Main.py.
It is the number of seconds away the AI can see new cars.
