from collections import deque
from Queue import Queue
from Queue import LifoQueue
from AStarQueue import AStarQueue

def DFS(init):
    #print("Performing Depth First Search")
    s = init
    openq = LifoQueue()
    openq.put(s)
    seen = set()
    counter = 0
    while not openq.empty():
        s = openq.get()
        counter += 1
        #print(s)
        if s.solved():
            break
        for i in reversed(s.actions()):
            if hash(i) not in seen:
                openq.put(i)
                seen.add(hash(i))
    #print("looked at {} nodes".format(counter))
    solution = []
    if s.solved():
        #print("Found solution!\n")
        while s is not None:
            solution = [s] + solution
            s = s.parent
    else:
        print("Could not find solution\n")
    return solution

def BFS(init):
    #print("Performing Breadth First Search")
    s = init
    openq = Queue()
    openq.put(s)
    seen = set()
    counter = 0
    while not openq.empty():
        s = openq.get()
        counter += 1
        #print(s)
        if s.solved():
            break
        for i in s.actions():
            if hash(i) not in seen:
                openq.put(i)
                seen.add(hash(i))
    #print("looked at {} nodes".format(counter))
    solution = []
    if s.solved():
        #print("Found solution!\n")
        while s is not None:
            solution = [s] + solution
            s = s.parent
    else:
        print("Could not find solution\n")
    return solution

def Hill(init):
    #print("Performing Hill Climbing Search")
    s = init
    counter = 0
    while s and not s.solved():
        counter += 1
        #print(s)
        if s.solved():
            break
        bestaction = None
        for i in s.actions():
            if not bestaction:
                bestaction = i
            else:
                if i.hcost < bestaction.hcost:
                    bestaction = i
        s = bestaction
    #print("looked at {} nodes".format(counter))
    solution = []
    if s and s.solved():
        #print("Found solution!\n")
        while s is not None:
            solution = [s] + solution
            s = s.parent
    else:
        print("Could not find solution\n")
    return solution


printastar = False

def Astar(init):
    #print("Performing A* Search")
    s = init
    openq = AStarQueue()
    openq.add(s)
    counter = 0
    while openq:
        #print(len(openq))
        #if counter%500 == 0:
        #    print(len(openq))
        if printastar:
            print("\n==================\n======START=======\n==================\n\n")
            print(openq)
            print("\n==================\n")
        s = openq.pop()
        if printastar:
            print("\n\nRetrieved:")
            print(s)
            print("\n==================\n")
        s.open = False
        counter += 1
        if s.solved():
            break
        s.addancestors()
        #if printastar:
            #print("\nChildren:")
        for i in s.actions():
            #if printastar:
                #print(i)
                #print("cost: {}\n".format(i.cost))
            if hash(i) not in s.ancestors:
                if openq.add(i):
                    s.addsuccessor(i)
        if printastar:
            print("\n==================\n=======END========\n==================\n\n")
    #print("looked at {} nodes".format(counter))
    solution = []
    if s.solved():
        #print("Found solution!\n")
        while s is not None:
            solution = [s] + solution
            s = s.parent
    else:
        print("Could not find solution\n")
    return solution
                
                
