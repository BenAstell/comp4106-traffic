from random import random
from random import gauss


def generatetraffic(filename=None):
    numcars = raw_input("Number of cars:\n>> ")
    if not numcars:
        numcars = "100"
    numcars = int(numcars)

    arrivaltime = raw_input("Average time between car arrivals\n>> ")
    if not arrivaltime:
        arrivaltime = "2"
    arrivaltime = float(arrivaltime)

    fromnorthweight = float(raw_input("Weight for cars to come from north\n>> "))
    fromeastweight = float(raw_input("Weight for cars to come from east\n>> "))
    fromsouthweight = float(raw_input("Weight for cars to come from south\n>> "))
    fromwestweight = float(raw_input("Weight for cars to come from west\n>> "))

    fromtotal = fromnorthweight+fromeastweight+fromsouthweight+fromwestweight
    fromnorthprob = fromnorthweight/fromtotal
    fromeastprob = fromeastweight/fromtotal
    fromsouthprob = fromsouthweight/fromtotal
    fromwestprob = fromwestweight/fromtotal

    tonorthweight = float(raw_input("Weight for cars to head to north\n>> "))
    toeastweight = float(raw_input("Weight for cars to head to east\n>> "))
    tosouthweight = float(raw_input("Weight for cars to head to south\n>> "))
    towestweight = float(raw_input("Weight for cars to head to west\n>> "))

    tototal = tonorthweight+toeastweight+tosouthweight+towestweight
    tonorthprob = tonorthweight/tototal
    toeastprob = toeastweight/tototal
    tosouthprob = tosouthweight/tototal
    towestprob = towestweight/tototal

    cars = []
    time = 0
    for i in range(numcars):
        time += int(round(gauss(arrivaltime,arrivaltime)))
        if time < 0:
            time = 0
        fromdirection = ""
        todirection = ""
        randnum = random()
        if randnum < fromnorthprob:
            fromdirection = "n"
            todirection = "n"
        elif randnum < fromnorthprob+fromeastprob:
            fromdirection = "e"
            todirection = "e"
        elif randnum < fromnorthprob+fromeastprob+fromsouthprob:
            fromdirection = "s"
            todirection = "s"
        else:
            fromdirection = "w"
            todirection = "w"
            
        while fromdirection == todirection:
            randnum = random()
            if randnum < tonorthprob:
                todirection = "n"
            elif randnum < tonorthprob+toeastprob:
                todirection = "e"
            elif randnum < tonorthprob+toeastprob+tosouthprob:
                todirection = "s"
            else:
                todirection = "w"
        cars += ["{}{}{}".format(time,fromdirection,todirection)]

    if filename:
        with open(filename, "wb") as f:
            f.write(",".join(cars))
    return cars


filename = raw_input("input file name:\n>> ")
if not filename:
    filename = "traffic.csv"
generatetraffic(filename)
