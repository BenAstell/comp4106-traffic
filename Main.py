import Traffic

from datetime import datetime
import Searches
from Node import Node

def printsolution(sol):
    for x in sol:
        print(x.state.time)
        print(x)


omniscientsearch = True

#How many seconds away can the AI see a car coming?
perception = 5

initstate = Traffic.getinit()
initnode = Node(initstate, 1, None, "Init")
#print(initnode)
intesolution = [initnode]
omnisolution = []
hillsolution = []
dumbsolution = [initnode]
currstate = initstate
currnode = initnode

tintestart = datetime.now()
while not currnode.solved():
#if False:
    tempstate = Traffic.State(
        currnode.state.lanes.timecutoff(currnode.state.time+perception),
        currnode.state.config, 0, currnode.state.time)
    tempnode = Node(tempstate, 1, None, "TempInit")
    tempsolution = Searches.Astar(tempnode)
    if len(tempsolution) > 1:
        for n in currnode.actions():
            if n.action == tempsolution[1].action:
                intesolution += [n]
                currnode = n
                break
    else:
        currnode = currnode.actions()[0]
        intesolution += [currnode]
tinteend = datetime.now()

print("\nIntelligent Solution:\n=========")
print("Total Cost: {}".format(intesolution[-1].cost))
print("Total Time: {}".format(intesolution[-1].state.time))
print("Total time taken for intelligent solution: {} seconds".format((tinteend-tintestart).total_seconds()))

thillstart = datetime.now()
hillsolution = Searches.Hill(initnode)
thillend = datetime.now()

print("\nHill Climb Solution:\n=========")
print("Total Cost: {}".format(hillsolution[-1].cost))
print("Total Time: {}".format(hillsolution[-1].state.time))
print("Total time taken for default solution: {} seconds".format((thillend-thillstart).total_seconds()))


currnode = initnode
tdumbstart = datetime.now()
while not currnode.solved():
    newstate = currnode.state.vertical(10)
    newnode = Node(newstate[1], 1, currnode, newstate[0])
    dumbsolution += [newnode]
    currnode = newnode
    if currnode.solved():
        break
    newstate = currnode.state.horizontal(10)
    newnode = Node(newstate[1], 1, currnode, newstate[0])
    dumbsolution += [newnode]
    currnode = newnode
tdumbend = datetime.now()

print("\nDefault Solution:\n=========")
print("Total Cost: {}".format(dumbsolution[-1].cost))
print("Total Time: {}".format(dumbsolution[-1].state.time))
print("Total time taken for default solution: {} seconds".format((tdumbend-tdumbstart).total_seconds()))


if omniscientsearch:
    tomnistart = datetime.now()
    omnisolution = Searches.Astar(initnode)
    tomniend = datetime.now()

    print("\nOmniscient Solution:\n=========")
    print("Total Cost: {}".format(omnisolution[-1].cost))
    print("Total Time: {}".format(omnisolution[-1].state.time))
    print("Total time taken for omniscient solution: {} seconds".format((tomniend-tomnistart).total_seconds()))


anything = raw_input("input anything to see the solution paths:\n>> ")
if anything:
    print("\nIntelligent Solution:\n=========")
    printsolution(intesolution)
    print("\nHill Climbing Solution:\n=========")
    printsolution(hillsolution)
    print("\nDefault Solution:\n=========")
    printsolution(dumbsolution)
    if omniscientsearch:
        print("\nOmniscient Solution:\n=========")
        printsolution(omnisolution)
